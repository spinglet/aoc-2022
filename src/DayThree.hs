module DayThree ( dayThree ) where

import Data.Char (isUpper, ord)

dayThree :: String -> (Integer, Integer)
dayThree fileContents =
  let rucksacks = lines fileContents

  in ( sum (map processRucksack rucksacks)  , sum ( map processSafetyGroup (groupRucksacks rucksacks )))

processRucksack :: String -> Integer
processRucksack rucksack =
  let (compartment1, compartment2) = splitAt (div (length rucksack)  2 ) rucksack
      duplicate = head [ x | x <- compartment1, x `elem` compartment2 ]
  in getValueDuplicate duplicate


getValueDuplicate :: Char -> Integer
getValueDuplicate value = if isUpper value then toInteger (ord value) - (65 - 27) else toInteger (ord value) - (97 - 1)

groupRucksacks :: [String] -> [(String, String, String)]
groupRucksacks (x : y : z : xs) = [(x,y,z)] ++ groupRucksacks xs
groupRucksacks (x : y : z : [] ) = [(x,y,z)]
groupRucksacks _ = []

processSafetyGroup :: (String, String, String) -> Integer
processSafetyGroup (s1, s2, s3) =
  let duplicate =  head [ x | x <- s1, x `elem` s2, x `elem` s3 ]
  in getValueDuplicate duplicate
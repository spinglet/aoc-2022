module DayOne ( dayOne ) where

import Data.List (sort)
import Data.List.Split (splitOn)
import Data.Maybe (mapMaybe)
import Text.Read (readMaybe)

-- Split the file contents into each elf's string
-- Create a list of total calories per elf
-- Sort calorie total list by size
-- Return (highest, sum of three highest)
dayOne :: String -> (Integer, Integer)
dayOne filecontents =
  let perElfCalorieStrings = splitOn "\n\n" filecontents
      perElfCalorieTotals  = map sumCalorieList perElfCalorieStrings
      threeHighestCalories = take 3 ((reverse . sort) perElfCalorieTotals)
  in  (maximum perElfCalorieTotals, sum threeHighestCalories)

-- Split an elf's snack list into individual snacks
-- Convert to integers then sum
sumCalorieList :: String -> Integer
sumCalorieList calorieString =
  let snacks = lines calorieString
  in  sum (mapMaybe snacksToInt snacks)

-- Convert from string to integer
snacksToInt :: String -> Maybe Integer
snacksToInt snack = readMaybe snack :: Maybe Integer

module DayThirteen (dayThirteen, isCorrect) where

import Data.List.Split (splitOn)
import Text.Read (readMaybe)
import Debug.Trace

dayThirteen :: String -> (Int, Int)
dayThirteen fileContents =
    let pairs = splitOn "\n\n" fileContents
        indices = zipWith (boolToInt) (map (isCorrect . take 2 . words) pairs) [1..]
    in  (sum indices, 0)

boolToInt :: Bool -> Int -> Int
boolToInt b i = if b then i else 0

isCorrect :: [String] -> Bool
isCorrect (pl : pr : _) = comparePair pl pr
isCorrect _ = error "wrong num of pairs"

comparePair :: String -> String -> Bool
comparePair pl pr
    | pl == "" , pr == "" = True
    | pl == "" = True
    | pr == "" = False
    | head pl == head pr = comparePair (tail pl) (tail pr)
    | head pl == ']' = comparePair (tail pl) (dropList pr)
    | head pr == ']' = False
    | head pl == '[', head pr == ']' = False
    | head pl == '['  = trace (show (makeList pr)) comparePair pl (makeList pr)
    | head pr == '[' = comparePair (makeList pl) pr
    | otherwise = compareOnInt pl pr

makeList :: String -> String
makeList s =
    let integer = head $ splitOn "," $ head $ splitOn "]" $ s
        commaSplit = takeWhile (/= ',') s
        endListSplit = takeWhile (/= ']') s
    in  if (length commaSplit < length endListSplit) then
                "[" ++ commaSplit ++ "]" ++ (drop (length commaSplit) s)
           else "[" ++ endListSplit ++ "]" ++ (drop (length endListSplit) s)
    

dropList :: String -> String
dropList s =
    tail $ dropWhile (/= ']') s

compareOnInt :: String -> String -> Bool
compareOnInt pl pr =
    let (x : xs) = map (splitOn ",") $ splitOn "]" $ pl
        (y : ys) = map (splitOn ",") $ splitOn "]" $ pr
        xval = trace ("x" ++ show x) readMaybe (head x) :: Maybe Int
        yval = trace ("y" ++ show y) readMaybe (head y) :: Maybe Int
    in  case xval of
            Just x' -> case yval of 
                           Just y' -> if x' == y' then comparePair (concat (tail x) ++ (concat $ concat xs)) (concat (tail y) ++ (concat $ concat ys)) else if x' < y'  then True else False

                           _       -> False
            _ -> False

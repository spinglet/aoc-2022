{-# LANGUAGE BangPatterns #-}
module DayTwelve (dayTwelve)
--, Square(..), parseHeights, makeStep) 
where

import Data.Maybe (mapMaybe, fromJust)
import Data.List (sort, foldl')
import Data.Char (ord)
import Debug.Trace
import Data.Tuple (swap)
import qualified Data.Sequence as Seq
import qualified Data.Set as S
import qualified Data.Map.Strict as M

type CoOrd = (Int, Int)

-- Return a set of all positions on grid, and a map of position and the value there
coOrdsAndValues :: String -> (S.Set CoOrd, M.Map CoOrd Char)
coOrdsAndValues input =
  foldr accum (S.empty, M.empty) $ do
      (y, line) <- zip [0..] $ lines input
      (x, val ) <- zip [0..] line
      [((x,y), val)]
  where
    accum (p, val') (coords, values) = (S.insert p coords, M.insert p val' values)

squaresAround :: M.Map CoOrd Char -> S.Set CoOrd -> CoOrd -> [CoOrd]
squaresAround m valid (x, y) =
    filter (isValid m (x, y)) $ filter (`S.member` valid) [(x+1, y), (x-1,y), (x,y+1), (x,y-1)]

isValid :: M.Map CoOrd Char -> (Int, Int) -> (Int, Int) -> Bool
isValid m p1 p2 =
  let v1 = M.findWithDefault 'a' p1 m
      v2 = M.findWithDefault 'a' p2 m
  in  case v2 of
        'E' -> ord v1 >= (ord 'z' -1)
        _   -> (v1 == 'S' && (ord 'a' >= ord v2)) || ((ord v1) >= (ord v2-1))


shortestPath :: (S.Set CoOrd, M.Map CoOrd Char) -> CoOrd -> CoOrd -> Maybe Int
shortestPath (coords, valueMap) start end =
    search (S.singleton start) (Seq.singleton (start, 0)) end
    where
      search visited toVisit end =
        case Seq.viewl toVisit of
          Seq.EmptyL -> Nothing
          (at, d) Seq.:< rest ->
            let
              neighbours = squaresAround valueMap (coords S.\\ visited) at
              visited' = S.union visited $ S.fromList neighbours
              moreToTry = Seq.fromList $ map (\n -> (n, d+1)) neighbours
            in
              if at == end
              then Just d
              else search visited' (rest Seq.>< moreToTry) end

getStartEnd :: (M.Map CoOrd Char) -> (CoOrd, CoOrd, [CoOrd])
getStartEnd ccmap =
    let ccmapList = map swap $ M.toList ccmap
        revMap = M.fromList ccmapList
        start = M.findWithDefault (0,0) 'S' revMap
        end = M.findWithDefault (0,0) 'E' revMap
        starts = map snd (filter (\p -> fst p == 'a') ccmapList)
    in (start, end, starts)

dayTwelve :: String -> (Int, Int)
dayTwelve fileContents =
    let (coords, valueMap) = coOrdsAndValues fileContents
        (start, end, starts) = getStartEnd valueMap
        maybeP1 = fromJust (shortestPath (coords, valueMap) start end)
        p2s = mapMaybe (\s -> shortestPath (coords, valueMap) s end) starts
    in  (maybeP1, minimum p2s)


-- let (start, maze, end) = parseHeights fileContents
--         iterates = (length maze)
--         allPaths = foldl (takePathsAddSteps maze) [[Square start 'a']] [1..(length maze)]
--         pathsToEnd = filter hitsEnd allPaths
--         pathLength = length (head pathsToEnd)
--     in  (pathLength - 1, 0)
-- 
-- hitsEnd :: [Square] -> Bool
-- hitsEnd ss = 'E' `elem` map squareHeight ss
-- 
-- data Square = Square
--   { squareLocation :: !(Int, Int)
--   , squareHeight :: !Char
--   } deriving ( Eq, Show)
-- 
-- takePathsAddSteps :: [Square] -> [[Square]] -> Int ->[[Square]]
-- takePathsAddSteps grid paths stepNumber =
--     -- in each step, take all paths, add a step and maybe remove
--     trace ("path num: " ++ (show stepNumber) ++ show (length paths)) concatMap (makeStep paths grid) paths 
-- 
-- -- Given a square, and the grid, return squares touching
-- getSquaresAround :: [Square] -> Square -> [Square]
-- getSquaresAround ss (Square (sx, sy) _) =
--     let squareAbove = filter (\s -> squareLocation s == (sx, sy+1)) ss
--         squareBelow = filter (\s -> squareLocation s == (sx, sy-1)) ss
--         squareLeft  = filter (\s -> squareLocation s == (sx-1, sy)) ss
--         squareRight = filter (\s -> squareLocation s == (sx+1, sy)) ss
--     in  squareAbove <> squareBelow <> squareLeft <> squareRight
-- 
-- -- get start, maze, and heights. Turn char into num
-- parseHeights :: String -> ((Int, Int), [Square], (Int, Int))
-- parseHeights input =
--     let rows = zip [1..(length input)] (lines input) -- [(1, r1), (2, r2),..]
--         sq   = concatMap processRow rows
--         st   = squareLocation $ head (filter (\s -> squareHeight s == 'S') sq)
--         end  = squareLocation $ head (filter (\s -> squareHeight s == 'E') sq)
--     in  (st, sq, end)
-- 
-- processRow :: (Int, [Char]) -> [Square]    
-- processRow (rowNum, rowContents) =
--         zipWith (makeSquare rowNum) rowContents [1..]
-- 
-- makeSquare :: Int -> Char -> Int -> Square
-- makeSquare rn rc cn = Square (rn, cn) rc
-- 
-- addStep :: [Square] -> Square -> Maybe [Square]
-- addStep path sq@(Square (sx, sy) sh)
--     | sq `elem` path = Nothing
--     | otherwise = Just (path ++ [sq])
-- 
-- -- grid, path returning new paths
-- makeStep :: [[Square]] -> [Square] -> [Square] -> [[Square]]
-- makeStep visited maze path =
--     case squareHeight (last path) of
--     'E' -> [path]
--     _   -> let options = getSquaresAround maze (last path)
--            in  mapMaybe (addStep path) (filter (unvisited visited) (filter (heightOk (last path)) options))
-- 
-- unvisited :: [[Square]] -> Square -> Bool
-- unvisited ss s = s `notElem` (concat ss)
-- 
-- heightOk :: Square -> Square -> Bool
-- heightOk (Square _ sh1) (Square _ sh2) =
--    case sh2 of
--      'E' -> ord sh1 >= (ord 'z' -1)
--      _   -> (sh1 == 'S' && (ord 'a' >= ord sh2)) ||( (ord sh1) >= (ord sh2-1))
-- 

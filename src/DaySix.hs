module DaySix (daySix) where

import Data.List.Unique (allUnique)

daySix :: String -> (Int, Int)
daySix fileContents =
    let initialLength = length fileContents
        message1 = fromMarker fileContents 4
        message2 = fromMarker fileContents 14
    in  (initialLength - (length message1), initialLength - (length message2))

fromMarker :: String -> Int -> String
fromMarker message markerLength =
    if allUnique (take markerLength message)
    then drop markerLength message
    else fromMarker (tail message) markerLength


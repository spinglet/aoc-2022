module DayFifteen (dayFifteen) where
import qualified Data.Map as M
import Data.Maybe (fromMaybe, mapMaybe)
import Text.Read (readMaybe)
import Data.List.Split (splitOn)
import Data.List (sortOn)
import qualified Data.Set as S
import Debug.Trace

type Coord = (Integer, Integer)

dayFifteen :: String -> (Integer, Integer)
dayFifteen fileContents = 
  let sourceBeacons = map parseLine $ lines fileContents
      rs = zip (map (rowRanges sourceBeacons) [0..4000000]) [0..4000000] -- [(mi,ma)]
      rs' = filter (\x -> fst x /= [(0,4000000)]) rs
      (xs, y) = head rs'
      x = 1 + ( snd $ head xs )
  in  trace (show rs') (4000000*x+y, 0)

-- sensor, closest
parseLine :: String -> (Coord, Coord)
parseLine s =
  let sx = readInt (takeWhile (/=',') ((splitOn "x=" s) !! 1))
      sy = readInt (takeWhile (/=':') ((splitOn "y=" s) !! 1))
      bx = readInt (takeWhile (/=',') ((splitOn "x=" s) !! 2))
      by = readInt (takeWhile (/=':') ((splitOn "y=" s) !! 2))
  in  ((sx,sy), (bx,by))

readInt :: String -> Integer
readInt s = fromMaybe 0 (readMaybe s :: Maybe Integer)

-- For a row number (y coord), return range where it cannot be
rowRanges :: [(Coord, Coord)] -> Integer -> [(Integer, Integer)]
rowRanges sNb rN =
  let notOk = mergeRanges $ sortOn fst $ (mapMaybe (xRange rN) sNb)
  in  trace ("rn: " ++ show rN ++ show notOk) notOk

-- List of ranges, add a new range
catRanges :: [(Integer, Integer)] -> (Integer, Integer) -> [(Integer, Integer)]
catRanges rs (a, b) =
  let addR = sortOn fst $ map (addRange (a,b)) rs 
  in  if addR == rs then rs ++ [(a,b)] else  addR

mergeRanges :: [(Integer, Integer)] -> [(Integer, Integer)]
-- (21,40),(0,9),(14,36),(1,20)
mergeRanges ((l1, h1) : (l2, h2) : rest)
  | h1 >= l2 - 1= mergeRanges ((l1, maximum[h1,h2]):rest)
mergeRanges (interval:rest) = interval : mergeRanges rest
mergeRanges [] = []

-- (1, 4) (5, 7)
-- Maybe add a range to another range -- default to first
addRange :: (Integer,Integer) -> (Integer, Integer) -> (Integer, Integer)
addRange (min1,max1) (min2,max2) 
  | min2 > max1 || max2 < min1 = (min1,max1) --nooverlap
addRange (min1,max1) (min2, max2) =
  let min = minimum [min1, min2]
      max = maximum [max1, max2]
  in (min, max)
  
-- For a row number (ycoord) and a (source, beacon), return x range of bits that source wipes out
xRange :: Integer -> (Coord, Coord) -> Maybe (Integer, Integer)
xRange rN (s@(sx, sy), b@(bx, by)) =
  let sd = abs (sx-bx) + abs (sy-by)
      
      xrange = sd - abs (sy - rN) 
      xmin = maximum [0, sx - xrange]
      xmax = minimum [4000000, sx + xrange]
  in  if abs (sy - rN) > sd then Nothing else Just (xmin, xmax) --trace ("source: " ++ show s ++ "beacon: " ++ show b ++ "row: " ++ show rN ++ "adding: " ++ show xmin ++ ", " ++show xmax) Just (xmin, xmax)

-- fillInRow :: [(Coord, Coord)] -> Integer -> S.Set Integer
-- fillInRow sourceNBases rN =
--   let rC = S.fromList [1..4000000]
--       notOk = foldl (rowOneSource rN) S.empty (sourceNBases)
--   in  rC S.\\ notOk
-- 
-- rowOneSource :: Integer -> S.Set Integer -> (Coord, Coord) -> S.Set Integer
-- rowOneSource rN cSet (s@(sx, sy), b@(bx, by)) =
--   let sd = abs (sx-bx) + abs (sy-by)
--       xrange = sd - abs (sy - rN)
--       notIn = S.fromList [sx - xrange .. sx + xrange]
--   in  S.union cSet notIn
-- 
-- fillX :: Integer -> Integer -> Integer -> [Coord]
-- fillX dist y s@(sx,sy) =
--    let xValRange = dist - abs (sy - y)
--        notIn =  S.fromList [sx - xValRange .. sx + xValRange]
--    in 
-- 
-- fillOutCoordMap :: Integer -> M.Map Coord Char -> (Coord, Coord) -> M.Map Coord Char
-- fillOutCoordMap rN cMap (s@(sx,sy), b@(bx,by)) =
--   let shortDist = abs (sx-bx) + abs (sy-by)
--       cMap' = M.insert s 'S' cMap
--       cMap'' =  M.insert b 'B' cMap'
--       addMap = M.fromList (zip (getXdist shortDist rN s) (replicate 100000000 '#'))
--   in  if rN `elem` [sy-shortDist .. sy+shortDist] then M.union cMap'' addMap else cMap
-- --       maybeAddToMap = M.fromList (zip (filter (\x -> snd x == rN) (getAllDist shortDist s b)) (replicate 1000000 '#'))
-- --   in  if rN `elem` [sy-shortDist .. sy+shortDist] then M.union cMap'' maybeAddToMap else cMap
-- 
-- fillOutCoordMap2 :: Integer -> M.Map Coord Char -> (Coord, Coord) -> M.Map Coord Char
-- fillOutCoordMap2 rN cMap (s@(sx,sy), b@(bx,by)) =
--   let shortDist = abs (sx-bx) + abs (sy-by)
--       maybeAddToMap = M.fromList (zip ((getAllDist shortDist s b)) (replicate 1000000 '#'))
--   in  M.union cMap maybeAddToMap 
-- 
-- -- addNoGoCoords :: S.Set Coord -> (Coord, Coord) -> S.Set Coord
-- -- addNoGoCoords rN sigSet (s@(sx, sy), b@(bx,by)) =
-- --   let sd = abs (sx - bx) + abs (sy - by)
-- --       maybeAddToMap = S.fromList (getAllDist sd s b)
-- --   in  S.union sigSet maybeAddToMap
-- 
-- getXdist :: Integer -> Integer -> Coord -> [Coord]
-- getXdist dist y s@(sx,sy) =
--    let xValRange = dist - abs (sy - y)
--    in  map (\x -> (x,y)) [sx - xValRange .. sx + xValRange]
-- 
-- getAllDist :: Integer -> Coord -> Coord -> [Coord]
-- getAllDist dist s@(sx, sy) b@(bx,by) =
--   let rowToAdd = [maximum [0, sx-dist] .. minimum [sx +dist, 4000000]]
--   in  concatMap (addYCoord dist sx sy) (rowToAdd)
--  where addYCoord d sx' sy' x' = map (\y -> (x',y)) [maximum [0, sy-(d - abs (sx - x'))] ..  minimum [4000000, sy+ (d - abs (sx - x'))]]

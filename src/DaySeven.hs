module DaySeven (daySeven) where

import Data.List (isPrefixOf, nub)
import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)
import Debug.Trace

data File = File
  { fileName :: !String
  , fileSize :: !Int
  } deriving (Show)

data Folder = Folder
  { folderName :: !String
  , folderSize :: !Int
  } deriving (Show)

-- Take filesystem and instruction, return new filesystem and pwd
processLine :: ([File], String) -> String -> ([File], String)
processLine (files, pwd) line =
  case head line of
    '$' -> 
            (files, (processCommand pwd line))
    _   ->
      case processFile pwd line of
        Just newFile -> (newFile : files, pwd)
        Nothing -> (files, pwd)

processFile :: String -> String -> Maybe File
processFile pwd item =
  let parts = words item
  in  case parts of
      (size : name : _) -> Just (File (pwd <> name) (readInteger size))
      [] -> Nothing

readInteger :: String -> Int
readInteger s = fromMaybe 0 (readMaybe s :: Maybe Int)

processCommand :: String -> String -> String
processCommand pwd "$ ls" = pwd
-- can now assume all other commands are cd, outside of advent of code I'd check that
processCommand pwd command =
  changeDir pwd (snd (splitAt 5 command))

changeDir :: String -> String -> String
changeDir pwd ".." = reverse (dropWhile (/= '/') (drop 1 (reverse pwd)))
changeDir pwd newFolder =
  case newFolder of
    "/" -> newFolder
    _   -> pwd <> newFolder ++ "/"


getFolderSize :: [File] -> String -> Int
getFolderSize allFiles path =
    let relFiles = filter (\x -> path `isPrefixOf` (fileName x)) allFiles
    in  sum (map fileSize relFiles)

getFolderNames :: [String] -> [String]
getFolderNames allFiles =
  let folderNames = map dropFileName allFiles
  in  nub folderNames
 where
   dropFileName fp = reverse  (dropWhile (/= '/') (reverse fp))

daySeven :: String -> (Int, Int)
daySeven fileContents = 
    let instructions = lines fileContents
        (files, _)   = foldl processLine ([],"/") instructions
        filePaths    = map fileName files
        folderNames  = getFolderNames filePaths
        folderSizes  = map (getFolderSize files) folderNames
        foldersUnderLimit = filter (< 100001) folderSizes
        
        totalFileSize = getFolderSize files "/"
        spaceNeeded =  30000000 - (70000000 - totalFileSize)
        
        foldersBigEnough = filter (> spaceNeeded) folderSizes
        smallest = minimum foldersBigEnough
    in  (sum foldersUnderLimit, smallest)



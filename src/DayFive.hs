module DayFive (dayFive) where

import Data.List (transpose)
import Data.Maybe (fromMaybe)
import Data.String.Utils (strip)
import Text.Read (readMaybe)

-- Each stack is a string with every entry a character
data CrateStack = CrateStack
  { stackNumber :: !Integer
  , stackContents :: !String
  } deriving (Show)

dayFive :: String -> (String, String)
dayFive fileContents =
    let (stackDiagram, instructionList) = break (== "") (lines fileContents)
        crateStacks =  parseStackInput (stackDiagram)
        movedCrateStacks = foldl applyInstruction crateStacks (drop 1 instructionList)
        movedCrateStacks2 = foldl applyInstruction2 crateStacks (drop 1 instructionList)
    in  (concatMap getHighestCrate movedCrateStacks, concatMap getHighestCrate movedCrateStacks2)

-- from string of stack numbers
createEmptyStacks :: String -> [CrateStack]
createEmptyStacks crateNumString =
    let cNums = words crateNumString
    in  map (\x -> CrateStack (readInteger x) []) cNums

-- from stack diagraming, ending on row number
parseStackInput :: [String] -> [CrateStack]
parseStackInput stackStrings =
    let rowNums = head (reverse stackStrings)
        stacks  = transpose (drop 1 (reverse stackStrings))
        initStacks = createEmptyStacks rowNums
    in  map (fillStack stacks) initStacks

-- fill in a crate stack from columns of input
fillStack :: [String] -> CrateStack -> CrateStack
fillStack stacks crate =
    let indexNum = 1 + 4 * ((stackNumber crate) - 1)
    in  crate { stackContents = strip (stacks !! (fromIntegral indexNum)) }

-- add to a cratestack
addToCrateStack :: String -> Integer -> [CrateStack] -> [CrateStack]
addToCrateStack addition stack crateStacks =
    map (\(CrateStack num list) -> if num == stack then CrateStack num (list <>(reverse addition)) else CrateStack num list) crateStacks

addToCrateStack2 :: String -> Integer -> [CrateStack] -> [CrateStack]
addToCrateStack2 addition stack crateStacks =
    map (\(CrateStack num list) -> if num == stack then CrateStack num (list <> addition) else CrateStack num list) crateStacks

-- remove from a cratestack
removeFromCrateStack :: Integer -> Integer -> [CrateStack] -> [CrateStack]
removeFromCrateStack takeAway stack crateStacks =
    map (\(CrateStack num list) -> if num == stack then CrateStack num (reverse (drop (fromIntegral takeAway) (reverse list))) else CrateStack num list) crateStacks

-- get moving crates
getCratesFromStack :: Integer -> Integer -> [CrateStack] -> [Char]
getCratesFromStack takeAway stack crateStacks =
    let movingStack = head [contents | CrateStack num contents <- crateStacks, num == stack]
    in  reverse (take (fromIntegral takeAway) (reverse movingStack))

-- process instructions in form move x from y to z
processInstruction :: String -> (Integer, Integer, Integer)
processInstruction ins =
    let parts = words ins
        x = readInteger (parts !! 1)
        y = readInteger (parts !! 3)
        z = readInteger (parts !! 5)
    in (x,y,z)

applyInstruction :: [CrateStack] -> String -> [CrateStack]
applyInstruction crates instruction=
    let (moveNum, moveFrom, moveTo) = processInstruction instruction
        cratesToMove = getCratesFromStack moveNum moveFrom crates
    in  removeFromCrateStack moveNum moveFrom (addToCrateStack cratesToMove moveTo crates)


applyInstruction2 :: [CrateStack] -> String -> [CrateStack]
applyInstruction2 crates instruction=
    let (moveNum, moveFrom, moveTo) = processInstruction instruction
        cratesToMove = getCratesFromStack moveNum moveFrom crates
    in  removeFromCrateStack moveNum moveFrom (addToCrateStack2 cratesToMove moveTo crates)
readInteger :: String -> Integer
readInteger s = fromMaybe 0 (readMaybe s :: Maybe Integer)

getHighestCrate :: CrateStack -> [Char]
getHighestCrate (CrateStack _ c) = [last c]

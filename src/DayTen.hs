module DayTen (dayTen, processLine, getValue) where

import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)

--get file return answers
dayTen :: String -> (Int, [String])
dayTen fileContents =
    let xValues = foldl processLine [1] (lines fileContents)
        clockCycles = [0 .. 39]
        -- split clock cycles into lines of 40
        -- cycle values = [1 .. 40]
        -- zipWith position (i.e. clockcycles) 
        -- for line: if cyclevalue in (position (-1 -> +1) then '#' else .
        -- show lines
        screenLines = group 40 xValues
        output = map (makeLine clockCycles) screenLines
    in  (getValue xValues, output)

group :: Int -> [a] -> [[a]]
group _ [] = []
group n l 
  | n > 0 = (take n l) : (group n (drop n l))
  | otherwise = error "uh oh"

-- take a cycle value (between 1 and 40) and a position
checkPosition :: Int -> Int -> Bool
checkPosition cycleNum spritePosition = (cycleNum == spritePosition) || (cycleNum == spritePosition + 1 ) || (cycleNum == spritePosition - 1)

-- for one row of [1..40] [spritePosition] make the draw line
makeLine :: [Int] -> [Int] -> String
makeLine = zipWith (\x y -> if checkPosition x y then '#' else '.')

-- from line read add or no add value to list
processLine :: [Int] -> String -> [Int]
processLine xs s =
    let currentX = last xs
    in  case words s of 
      ["noop"] -> xs ++ [currentX]
      ("addx": x : _) -> xs ++ [currentX] ++ [addNum x currentX]
      _ -> error "uh oh"

addNum :: String -> Int -> Int
addNum x xs = (fromMaybe 0 (readMaybe x :: Maybe Int)) + xs

-- from list get values
getValue :: [Int] -> Int
getValue xs = 
    let interesting = [20, 60, 100, 140, 180, 220]
    in  sum (map (\x -> (xs !! (x-1)) * x) interesting)

-- drawLine 

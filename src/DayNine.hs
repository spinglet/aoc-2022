{-# LANGUAGE BangPatterns #-}
module DayNine (dayNine,  readInstruction, firstBatch, nextBatch) where

import Data.Maybe (fromMaybe)
import Text.Read  (readMaybe)
import Data.List  (nub)
import Debug.Trace 

-- Take file contents, return answers
dayNine :: String -> (Int, Int)
dayNine fileContents = do
    let instructions = concatMap (readInstruction) (lines fileContents)
        headPos = (0, 0) : (zipWith moveHead headPos instructions)
        
        --coordsVisited prevCoords =  (0,0) : (zipWith tailNew prevCoords coordsVisited)
       -- stringCoordsVisited = (coordsVisited headPos) : (map coordsVisited stringCoordsVisited)
  
        vis = (coordsVisited headPos) : map coordsVisited vis
        -- coords2 =coordsVisited $!coordsVisited $!coordsVisited $!coordsVisited $!coordsVisited $!coordsVisited $! coordsVisited $! allCoordsVis1
    (length (nub (vis !! 0)), 0)

coordsVisited :: [(Int, Int)] -> [(Int, Int)]
coordsVisited h = foldl addTailNew [(0,0)] h

firstBatch :: String -> [(Int, Int)]
firstBatch s = let instructions = concatMap (readInstruction) (lines s)
                   headPos = (0, 0) : (zipWith moveHead headPos instructions)
               in headPos

nextBatch :: [(Int, Int)] -> [(Int, Int)]
nextBatch old = let newPos = (0, 0) : (zipWith tailNew old  newPos)
                in  newPos

addTailNew :: [(Int, Int)] -> (Int, Int) -> [(Int, Int)]
addTailNew co m = co ++ [tailNew m (last co)]

moveHead :: (Int, Int) -> Char -> (Int, Int)
moveHead (hx, hy) 'U' = (hx, hy + 1)
moveHead (hx, hy) 'D' = (hx, hy - 1)
moveHead (hx, hy) 'L' = (hx - 1, hy)
moveHead (hx, hy) 'R' = (hx + 1, hy)
moveHead _  _ = error "cant read"


tailNew :: (Int, Int) -> (Int, Int) -> (Int, Int)
tailNew (hx, hy) (tx, ty)
    | hx == (tx - 2) =  (tx - 1, maybeMove hy ty)
    | hx == (tx + 2) =  (tx + 1, maybeMove hy ty)
    | hy == (ty - 2) =  (maybeMove hx tx, ty - 1)
    | hy == (ty + 2) =  (maybeMove hx tx, ty + 1)
    | otherwise = (tx, ty) 

-- head position, tail position, new tail position
maybeMove :: Int -> Int -> Int
maybeMove a b
    | a == b - 1 = b - 1
    | a == b + 1 = b + 1
    | a == b - 2 = b - 1
    | a == b + 2 = b + 1
    | otherwise = b


-- Return a list of characters for a line of instruction
-- Repeat number (e.g. D 1 -> ['D']. L 2 -> ['L', 'L']
readInstruction :: String -> [Char]
readInstruction s =
    let move = head $ head (words s)
        num  = readInt (last (words s))
    in  replicate num move

readInt :: String -> Int
readInt s = fromMaybe 0 (readMaybe s :: Maybe Int)



module DayTwo ( dayTwo ) where


dayTwo :: String -> (Integer, Integer)
dayTwo fileContents =
  let games = lines fileContents
  in (sum ( map gameTotal games ), sum ( map game2Total games ))

--A for Rock, B for Paper, and C for Scissors
--X for Rock, Y for Paper, and Z for Scissors
--0 if you lost, 3 if the round was a draw, and 6 if you won
gameTotal :: String -> Integer
gameTotal "A X" = 4  -- 1+3
gameTotal "A Y" = 8  -- 2+6
gameTotal "A Z" = 3  -- 3+0
gameTotal "B X" = 1  -- 1+0
gameTotal "B Y" = 5  -- 2+3
gameTotal "B Z" = 9  -- 3+6
gameTotal "C X" = 7  -- 1+6
gameTotal "C Y" = 2  -- 2+0
gameTotal "C Z" = 6  -- 3+3
gameTotal _ = 0 -- default

--A for Rock, B for Paper, and C for Scissors
--X lose, Y draw, and Z  win
--0 if you lost, 3 if the round was a draw, and 6 if you won
game2Total :: String -> Integer
game2Total "A X" = 3  -- 3+0
game2Total "A Y" = 4  -- 1+3
game2Total "A Z" = 8  -- 2+6
game2Total "B X" = 1  -- 1+0
game2Total "B Y" = 5  -- 2+3
game2Total "B Z" = 9  -- 3+6
game2Total "C X" = 2  -- 2+0
game2Total "C Y" = 6  -- 3+3
game2Total "C Z" = 7  -- 1+6
game2Total _ = 0 -- default

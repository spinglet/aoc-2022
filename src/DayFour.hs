module DayFour (dayFour) where

import Text.Read (readMaybe)
import Data.Maybe (fromMaybe)
import Data.List.Split (splitOn)
import Data.Ix (range)

dayFour :: String -> (Integer, Integer)
dayFour fileContents =
  let pairs = lines fileContents
  in ( toInteger (length  (filter (==True) ( map processPair pairs ) )) , toInteger (length  (filter (==True) ( map processPairTwo pairs ) ))  )


processPair :: String -> Bool
processPair assignmentPairs =
  let numberRange = splitOn "," assignmentPairs
  in case numberRange of
    (rangeOne : rangeTwo : [] ) -> compareLists (boundsToArray rangeOne, boundsToArray rangeTwo)
    _ -> False

processPairTwo :: String -> Bool
processPairTwo assignmentPairs =
  let numberRange = splitOn "," assignmentPairs
  in case numberRange of
    (rangeOne : rangeTwo : [] ) -> listOverlaps (boundsToArray rangeOne) (boundsToArray rangeTwo)
    _ -> False



boundsToArray :: String -> [Integer]
boundsToArray assignmentPair =
  let numbers = splitOn "-" assignmentPair
  in case numbers of
    (lb : ub : [] ) ->  range (fromMaybe 0 ( readInteger lb ), fromMaybe 0 (readInteger ub))
    _ -> []
  where
  readInteger x = readMaybe x :: Maybe Integer

compareLists :: ([Integer], [Integer]) -> Bool
compareLists (listOne, listTwo) =
  listInList listOne listTwo || listInList listTwo listOne

listInList :: [Integer] -> [Integer] -> Bool
listInList listOne listTwo =
  and (map ( \x -> x `elem` listTwo) listOne)



listOverlaps :: [Integer] -> [Integer] -> Bool
listOverlaps listOne listTwo =
  or (map ( \x -> x `elem` listTwo) listOne)
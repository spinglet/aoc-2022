module DayEight (dayEight,newMax,beautyScore,treesInDirection) where

import Text.Read (readMaybe)
import Data.Maybe (fromMaybe)
import Data.List (transpose)
import Debug.Trace

dayEight :: String -> (Int, Int)
dayEight fileContents =
    let rows = lines fileContents
        grid = map (rowToTreeList) rows
        rNum = length grid
        cNum = length (head grid)
        coords = [(x, y) | x <- [1..(rNum)], y<-[1..(cNum)]]
        isVisGrid = (map (isVisible grid) coords)
        numTrue = length (filter (== True) isVisGrid)
        beautyScores = map (beautyScore grid) coords
    in  (numTrue, maximum beautyScores)

rowToTreeList :: [Char] -> [Int]
rowToTreeList = map readInteger

readInteger :: Char -> Int
readInteger c = fromMaybe 0 (readMaybe [c] :: Maybe Int)

isVisible :: [[Int]] -> (Int, Int) -> Bool
isVisible trees (r,c) =
    let (rl, rw) = splitAt c (trees !! (r-1))
        (cu, cd) = splitAt r ((transpose trees) !! (c-1))
        rl2 = reverse (tail (reverse rl))
        cu2 = reverse (tail (reverse cu))
        h = (trees !! (r-1)) !! (c-1)
    in  (newMax h rl2) || (newMax h rw) || (newMax h cu2) || (newMax h cd)

beautyScore :: [[Int]] -> (Int, Int) -> Int
beautyScore trees (r,c) =
   let (rl,_) = splitAt (c-1) (trees !! (r-1))
       (_, rr) = splitAt c (trees !! (r-1))
       (cu, _) = splitAt (r-1) ((transpose trees) !! (c-1))
       (_, cd) = splitAt r ((transpose trees) !! (c-1))
       h = (trees !! (r-1)) !! (c-1)
   in  foldr (*) 1 (map (treesInDirection h) [reverse rl, rr, reverse cu, cd])

--takes a list of trees from view (ie elem 0 is closest)
treesInDirection :: Int -> [Int] -> Int
treesInDirection h trees =
    let smallertrees = length (takeWhile (< h) trees)
        viewBlocked = if (newMax h trees) then 1 else 0
    in  smallertrees+viewBlocked

newMax :: Int -> [Int] -> Bool
newMax a [] = True
newMax a l = a > maximum l

module DayEleven (movesInListOfList, dayEleven, Monkey(mNumber, mHolding, mOp, mTest), monkeyRound, parseMonkey, getOp) where

import Data.List (isPrefixOf, sort)
import Data.List.Split (splitOn)
import Data.String.Utils (strip)
import Data.Maybe (fromMaybe, catMaybes)
import Text.Read (readMaybe)
import Debug.Trace
-- rounds + monkeys 

data Monkey = Monkey
  { mNumber :: !Int
  , mHolding :: ![Integer]
  , mOp :: !(Integer -> Integer)
  , mTest :: !(Integer -> Int)
  , mTestNum :: !Integer
  }

dayEleven :: String -> (Int, Int)
dayEleven fileContents =
    let start = map parseMonkey (splitOn "\n\n" fileContents)
        allRounds =  [start] : zipWith (monkeyRound) allRounds [1..]
        monkeyScores2 = map (sumScore (concat (take 10001 allRounds))) [0..((length start)-1)]
    in  (10605, product ( take 2 (reverse (sort monkeyScores2))))

-- monkey sum : get all moves made by one monkey
sumScore :: [[Monkey]] -> Int -> Int
sumScore monkeys mn =
    let moves = map (mHolding) (filter (\m -> mNumber m == mn) (concat monkeys))
    in  trace (show $ length (movesInListOfList moves) - 1) length (movesInListOfList moves) - 1

movesInListOfList :: [[Integer]] -> [[Integer]]
movesInListOfList [] = []
movesInListOfList l =
    let startingHand = head l
    in  startingHand : catMaybes (zipWith (\x y -> if length x == (length y) - 1 then Just x else Nothing) (tail l) (l))

-- [[ALL MONKEY STATES]] -> take last and play round [[ALL MONKEY STATES] : [NEW MONKEY ROUND]
monkeyRound :: [[Monkey]] -> Int -> [[Monkey]]
monkeyRound ms i = 
    let moveGroups = [last ms] : zipWith (monkeyTurn) moveGroups [0..(length (head ms)) -1]
    in  trace ("ROUND: " ++ show i) concat (tail moveGroups)

-- Add a new move to the list of monkey states. Operation for one monkey only.
monkeyTurn :: [[Monkey]] -> Int -> [[Monkey]]
monkeyTurn monkeysList i =
    let monkeys = last monkeysList
        monkeyInPlay = head ( filter (\m -> mNumber m == i) monkeys)
        monkeyItems = mHolding monkeyInPlay
        moves = monkeys : zipWith (handleItem monkeyInPlay) moves monkeyItems
    in  moves

handleItem :: Monkey -> [Monkey] -> Integer -> [Monkey]
handleItem mTurn msCurrent itemWorry =
    let testNumber = product (map mTestNum msCurrent)
        newItemWorry = ((mOp mTurn) itemWorry) `mod` testNumber
        thrownTo = (mTest mTurn) newItemWorry
--    in  trace ("Handling " ++ show itemWorry ++ " for " ++ show (mNumber mTurn) ++ ": throwing " ++ show newItemWorry ++ " to " ++ show thrownTo)
    in  map (playMove (mNumber mTurn) itemWorry newItemWorry thrownTo) msCurrent

playMove :: Int -> Integer -> Integer -> Int -> Monkey -> Monkey
playMove  mn  oldW newW throwTo  editMonkey@(Monkey mn1 mh1 mo1 mt1 mt2)=
    if mn == mn1 then Monkey mn (drop 1 mh1) mo1 mt1 mt2
    else if throwTo == mn1 then Monkey mn1 (mh1 ++ [newW]) mo1 mt1 mt2
    else editMonkey

parseMonkey :: String -> Monkey
parseMonkey input =
    let mN = getNum  (head (lines input))
        mH = getHold (head (drop 1 (lines input)))
        mO = getOp   (head (drop 2 (lines input)))
        (mT, mTN) = getTest ((drop 3 (lines input)))
    in  Monkey mN mH mO mT mTN

getNum :: String -> Int
getNum s =
    if "Monkey " `isPrefixOf` (strip s)
    then fromIntegral (readInt ((drop 7) (reverse (drop 1 (reverse s )))))
    else error $ "can't read"

getHold :: String -> [Integer]
getHold s =
    if "Starting items: " `isPrefixOf` (strip s)
    then let items = map strip (splitOn "," (concat (drop 2 (words s))))
         in  map readInt items
    else error $ "can't read"

getOp :: String -> (Integer -> Integer)
getOp s =
    if "Operation: new = old " `isPrefixOf` (strip s)
    then let op = head (drop 4 (words s))
             val =  last (words s)
         in  case val of
                 "old" -> square
                 _ -> doOp (readInt val) op
    else error $ "can't get op"

doOp :: Integer -> String -> Integer -> Integer
doOp v "*" = (v *)
doOp v "+" = (v +)
doOp v "/" = (v `div`)
dpOp v "-" = (v -)
 --   getOp _ = error $ "can't read integer"

square :: Integer -> Integer
square x = x * x

getTest :: [String] -> ((Integer -> Int), Integer)
getTest s =
    let testNum = readInt (last (words (head s)))
        trueThrow = fromIntegral (readInt (last (words (head (drop 1 s)))))
        falseThrow = fromIntegral (readInt (last (words (head (drop 2 s)))))
    in  ((\x -> if ((x `div` testNum) * testNum) == x then trueThrow else falseThrow), testNum)

readInt :: String -> Integer
readInt x = fromMaybe 0 (readMaybe x :: Maybe Integer)

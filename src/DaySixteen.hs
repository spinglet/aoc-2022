module DaySixteen (daySixteen) where

import Text.Read (readMaybe)
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe, fromJust, isJust)
import qualified Data.Set as S
import qualified Data.Map as M
import Data.List (foldl', (\\), permutations)
import Debug.Trace

type Node = String
type Flow = Int

data Move = Move
  { time :: !Int
--  , score :: !Int
  , position :: !Node
--  , nodesLeft :: !(S.Set Node)
  } deriving (Show, Eq)

daySixteen :: String -> (Int, Int)
daySixteen fileContents =
  let valves = map parseValve $ lines fileContents
      (nodeFM, nodeNN) = fillFlowMapNN valves
      t = 30
      valNodes = filter (/= "AA") $ filter (\x -> M.lookup x nodeFM /= Just 0) $ M.keys nodeFM
      possiblePaths = trace ("perms: " ++ show (valNodes)) permutations valNodes
      highestScore = foldl' (processPath nodeFM nodeNN t) 0 possiblePaths
--       finalMoves  = trace ("movin") makePaths nodeNN nodeFM "AA" 30
--       finalScores = trace (show finalMoves) maximum $ map score finalMoves
--   in  (finalScores, 0)
  in  (highestScore, 0)

processPath :: M.Map Node Flow -> M.Map Node [Node] -> Int -> Int -> [Node] -> Int
processPath nfm nnm t toBeat path =
  let moves = (Move 0 "AA") : zipWith (addMove nnm) moves (path)
      end   = takeWhile (\m -> time m < t) moves
      score = scoreUp nfm t end
  in  trace (show score) maximum [score, toBeat]

scoreUp :: M.Map Node Flow -> Int -> [Move] -> Int
scoreUp nmap t ms = sum $ map (moveToScore nmap) ms
 where
  moveToScore nmap m@(Move t' n) =
    let time = t - t'
        flow = fromJust $ M.lookup n nmap
    in  time * flow

-- Assume if you move to a node you are going to open it
addMove :: M.Map Node [Node] -> Move -> Node -> Move
addMove nnmap m@(Move t' p') nodeToAdd =
  let timediff = shortestPath nnmap p' nodeToAdd
  in  Move (t'+timediff) nodeToAdd

-- return nodename, nearest neighbours, flow
parseValve :: String -> (Node, [Node], Flow)
parseValve s =
  let splitBySemi = splitOn ";" s
      node =  (words (splitBySemi !! 0)) !! 1
      flow = readInt((splitOn "=" (splitBySemi !! 0)) !! 1)
      valves = splitOn ", " ((splitOn "valves " (splitBySemi !! 1) !! 1)) -- todo this brakes on valve (for now I just modified the input because sean is lazy! and tired)
  in (node, valves, flow)

readInt :: String -> Int
readInt s = fromMaybe 0 (readMaybe s :: Maybe Int)

-- Flow map and nearest neighbours
fillFlowMapNN :: [(Node, [Node], Flow)] -> (M.Map Node Flow, M.Map Node [Node])
fillFlowMapNN ns =
  let nf  = map (\(n,  _, i) -> (n, i)) ns
      nnn = map (\(n, ns, _) -> (n,ns)) ns
  in  (M.fromList nf, M.fromList nnn)

-- Shortest path between two points. All distances are one.
shortestPath :: M.Map Node [Node] -> Node -> Node -> Int
shortestPath nnmap nFrom nTo =
   -- We make a list of [list of Point] of visited from Start to End [[[MaybeNode]]]
   let pathsFromStart = foldl' (addStepsToPaths nnmap) [[Just nFrom]] [1.. length $ M.keys nnmap]
       pathsToEnd     = map (takeWhile (/=Just nTo)) $ filter (Just nTo `elem`) (map (takeWhile (/=Nothing)) pathsFromStart)
       pathLengths    = map length pathsToEnd
   in  (minimum pathLengths) 

-- take current paths, add next steps to all path [Maybe Node] paths [[Maybe Node]] 
-- pathsoversteps = [[[Maybe Node]]]
addStepsToPaths :: M.Map Node [Node] -> [[Maybe Node]] -> Int -> [[Maybe Node]]
addStepsToPaths nnm ns step = 
    let nseens = map fromJust $ filter isJust $ concat ns
    in  concatMap (addSteps nnm nseens) ns

-- take path, return path + next steps
addSteps :: M.Map Node [Node] -> [Node] -> [Maybe Node] -> [[Maybe Node]]
addSteps nnmap nseens ns 
 | last ns == Nothing = [ns]
 | otherwise =
    let now = last ns
        nns = filter (\n -> n `notElem` nseens) $ filter (\n -> n `notElem` map fromJust ns) (fromJust (M.lookup (fromJust now) nnmap))
    in  case nns of
          [] -> [ns ++ [Nothing]]
          nn -> map (\n -> ns ++ [Just n]) nn
-- 
-- --From start positoin -> return list of final move
-- makePaths :: M.Map Node [Node] -> M.Map Node Flow -> Node -> Int -> [Move]
-- makePaths pathMap flowMap start t = 
--   -- paths is a list of current state. [[Move0] : [Move1a, Move1b], ..]
--   let vNotZero = filter (\v -> M.lookup v flowMap /= Just 0) (M.keys flowMap)
--       paths    = foldl (addToPaths t flowMap pathMap) [(Move 0 0 start (S.fromList vNotZero))] [1..t]
--       --[(Move 0 0 start (S.fromList vNotZero))] : map (concatMap (addMoves t flowMap pathMap)) paths -- :: [[Move]] last list of move
--   in  paths
-- 
-- addToPaths :: Int -> M.Map Node Flow -> M.Map Node [Node] -> [Move] -> Int -> [Move]
-- addToPaths t fm pm cs step =
--   let complete = filter (\c -> time c == t) cs
--       stillGoing = cs \\ complete
--   in trace ("doing step " ++ show step) complete ++ concatMap (addMoves t fm pm) stillGoing
-- 
-- -- Next move options for a given move (returns same if time is up)
-- addMoves :: Int -> M.Map Node Flow -> M.Map Node [Node] -> Move -> [Move]
-- addMoves t flowmap pathMap current
--   | time current == t = trace ("at time") [current]
--   | nodesLeft current == S.empty = [takeToTime flowmap t current]
--   | otherwise =  trace (show $ time current) map (addMove flowmap pathMap t current) (S.toList $ nodesLeft current) ++ [takeToTime flowmap t current]
-- 
-- takeToTime :: M.Map Node Flow -> Int -> Move -> Move
-- takeToTime nfm t m@(Move t' s' p' nl') =
--   let timeLeft = t - t'
--       openValveFlow = sum $ map (\v -> fromJust $ M.lookup v nfm) (filter (`notElem` nl') (M.keys nfm))
--       extra = timeLeft * openValveFlow
--   in  Move t (s'+extra) p' nl'
-- 
-- -- Assume if you move to a node you are going to open it
-- addMove :: M.Map Node Flow -> M.Map Node [Node] -> Int -> Move -> Node -> Move
-- addMove nfm nnmap t m@(Move t' s' p' nl') nodeToAdd =
--   let timediff = shortestPath nnmap p' nodeToAdd
--       openValveFlow = sum $ map (\v -> fromJust $ M.lookup v nfm) (filter (`notElem` nl') (M.keys nfm))
--       s = s' + (timediff * openValveFlow)
--       nl = S.delete nodeToAdd nl'
--   in  if t' + timediff > t then takeToTime nfm t m else Move (t'+timediff) s nodeToAdd nl
-- 
-- 
-- 
-- 

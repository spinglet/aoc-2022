module DayFourteen (dayFourteen) where
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe, isJust, fromJust)
import Text.Read (readMaybe)
import Debug.Trace
import Data.List (intercalate)
type CoOrd = (Int, Int)
type Material = Char

-- Take input and make set of coords
parsePaths :: String -> (S.Set CoOrd, Int)
parsePaths input =
  let paths = lines input
      coords = concatMap getSteps paths
      caveStop = maximum (map snd coords)
  in  trace ("paths: " ++ show paths ++ "coords: " ++ show coords) ( S.fromList (coords), caveStop)

-- Get coords in one path
getSteps :: String -> [(Int, Int)]
getSteps s = -- a,b,c ... a -> b, b -> c
  let  pathCorners = map processPathCorners $ splitOn " -> " s -- [[oord]
       allPath = trace (show pathCorners) ( \x -> foldl addMiddleSteps [(head x)] (tail x)) pathCorners
  in   allPath
 where
  processPathCorners s' = let xs = splitOn "," s' in (readInt (head xs), readInt (head (drop 1 xs)))

addMiddleSteps :: [(Int, Int)] -> (Int, Int) -> [(Int, Int)]
addMiddleSteps steps next=
  let start = last steps
      additions = addMS start next
  in  trace (show start ++ show next ++ show additions) steps ++ tail additions
 where
  addMS (x, y) (x', y')
    | x == x', y >= y' = reverse $ map (\y'' -> (x,y'')) [y'..y]
    | x == x', y <  y' = map (\y'' -> (x,y'')) [y..y']
    | y == y', x <  x' = map (\x'' -> (x'',y)) [x..x']
    | y == y', x >= x' = reverse $ map (\x'' -> (x'',y)) [x'..x]
    | otherwise = error $ (show x ++ " " ++ show y ++ " " ++ show x' ++ " " ++ show y')


readInt :: String -> Int
readInt s = fromMaybe 0 (readMaybe s :: Maybe Int)      

-- Take the cave map of where everything is, and move grain of sand through it, returning the updated map
moveOneSand :: Int -> M.Map CoOrd Material -> Int -> M.Map CoOrd Material
moveOneSand cMax cmMap _=
   let  start = (500, 0)
        positions = Just start : zipWith (moveOneSandOne cmMap cMax) positions [1..]
        (xf, yf) = fromJust $ last $ takeWhile isJust positions
   -- in   if yf == cMax then cmMap else M.insert (xf,yf) 's' cmMap
   in   M.insert (xf, yf) 's' cmMap
   --in   trace( intercalate ("\n") (drawGrid cmMap)) M.insert (xf, yf) 's' cmMap -- THIS MIGHT NEED BE UPDATE

-- Take the map of where everything is, and the location of one grain of sand. Return the maybe updated position. Nothing if not updated or dropped off
-- Try down -> try down left right -> try down right
moveOneSandOne :: M.Map CoOrd Material -> Int -> Maybe CoOrd ->Int-> Maybe CoOrd
moveOneSandOne _ _ Nothing _= Nothing
moveOneSandOne cmMap maxY (Just c@(x,y)) _
  | y == maxY + 1 = Nothing
  | M.notMember (x, y+1) cmMap = Just (x, y+1)
  | M.notMember (x-1, y+1) cmMap = Just (x-1, y+1)
  | M.notMember (x+1, y+1) cmMap = Just (x+1, y+1)
  | otherwise = Nothing

drawGrid :: M.Map CoOrd Char-> [String]
drawGrid g = let numRows = [(minimum (map snd $ M.keys g)) .. (maximum (map snd $ M.keys g))]
                 numCols = [(minimum (map fst $ M.keys g)) .. (maximum (map fst $ M.keys g))]
             in  map (\rowNum -> map ((\x y -> M.findWithDefault '.' (y,x) g) rowNum) numCols) numRows


dayFourteen :: String -> (Int, Int)
dayFourteen fileContents =
  let (paths, cm) = parsePaths fileContents
      caveMap = M.fromList (zip (S.toList paths) (replicate (length paths) '#'))
      sandMaps = caveMap : zipWith (moveOneSand cm) sandMaps [1..] 
      -- dupSand = zipWith (/=) sandMaps (M.empty : sandMaps)
      diff = takeWhile (\s -> (500,0)`notElem` (M.keys s)) sandMaps
  in  (length diff -1, 0)
      -- zip with list of sand final coordinates (iterating through) 
      -- once two sand end in same place the rest have settled elsewhere
 

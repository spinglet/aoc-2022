import Test.Hspec

-- import DayOne (dayOne)
-- import DayTwo (dayTwo)
-- import DayThree (dayThree)
-- import DayFour (dayFour)
-- import DayFive (dayFive)
-- import DaySix (daySix)
-- import DaySeven (daySeven)
-- import DayEight (dayEight, newMax, beautyScore, treesInDirection)
-- import DayNine (dayNine,  readInstruction)
-- import DayTen (dayTen, processLine, getValue)
-- import DayEleven
-- import DayTwelve
-- import DayThirteen
import DayFifteen

import Data.List.Split (splitOn)


examplesDir :: String
examplesDir = "input/examples/"

main :: IO ()
main = hspec $ do
      it "day 15 aoc" $ do
        fileContents <- readFile (examplesDir <> "day15.txt")
        dayFifteen fileContents `shouldBe` (26, 0)


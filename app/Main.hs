module Main (main) where

import Options
-- import Data.List (nub)
-- import DayOne (dayOne)
-- import DayTwo (dayTwo)
-- import DayThree (dayThree)
-- import DayFour (dayFour)
-- import DayFive (dayFive)
-- import DaySix (daySix)
-- import DaySeven (daySeven)
-- import DayEight (dayEight)
-- import DayNine (dayNine, firstBatch, nextBatch)
-- import DayTen (dayTen)
-- import DayEleven (dayEleven)
-- import DayTwelve (dayTwelve)
-- import DayThirteen (dayThirteen)
--import DayFourteen (dayFourteen)
-- import DayFifteen (dayFifteen)
import DaySixteen (daySixteen)

data RunOptions = RunOptions
    { inputFolder :: String }

instance Options RunOptions where
    defineOptions = pure RunOptions
        <*> simpleOption "user" "examples"
            "Input subdirectory to use"

main :: IO ()
main = runCommand $ \opts _ -> do
    let inputDir = "input/" ++ (inputFolder opts) ++ "/"
    daySixteenContents <- readFile (inputDir ++ "day16.txt")
    let (p1, p2) = daySixteen (daySixteenContents)
    putStrLn ("Part 1: " ++ show p1)
    putStrLn ("Part 2: " ++ show p2)
--     putStrLn " ------ DAY FIFTEEN ------ "
--     dayFifteenContents <- readFile (inputDir ++ "day15.txt")
--     let (partOne, partTwo) = dayFifteen dayFifteenContents
--     putStrLn ("Part 1: " ++ show partOne)
--     putStrLn ("Part 2: " ++ show partTwo)
--     putStrLn " ------------------------- "

       
